package com.example.vadim.rss_reader.com.example.vadim.streams;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.AsyncTaskLoader;

import com.example.vadim.rss_reader.com.example.vadim.exception.FatalException;
import com.example.vadim.rss_reader.com.example.vadim.exception.IziException;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.parsing.HandleXML;
import com.example.vadim.rss_reader.com.example.vadim.internet.InternetConnection;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;


public class BackGround extends AsyncTaskLoader<ArrayList<ItemNews>> {
    private String mUrl;
    private DB mDataBase;
    private String mTableName;
    private Handler mHandler;
    private ArrayList<ItemNews> mItemsNews;
    private HandleXML mHandleXml;

    public BackGround(Context context, String url, String tableName, DB dataBase, Handler handler) {
        super(context);
        this.mUrl = url;
        this.mDataBase = dataBase;
        this.mTableName = tableName;
        this.mHandler = handler;
    }

    /**
     * Created object HandleXML, started receive food and
     * recorded in data base
     */
    @Override
    public ArrayList<ItemNews> loadInBackground() {
        mItemsNews = new ArrayList<>();
        mHandleXml = new HandleXML(mDataBase, mTableName);
        InternetConnection internetConnection = new InternetConnection(mUrl);

        try {
            InputStream connection = internetConnection.createConnection();
            mHandleXml.setParser(connection);
            internetConnection.closeConnection();
            getData();
        } catch (IziException iziException) {
            mItemsNews = mHandleXml.getItemsNews();
            if (mItemsNews.isEmpty() == true) {
                processingException(iziException.getMessage());
            } else {
                getData();
            }
        } catch (FatalException fatalException) {
            processingException(fatalException.getMessage());
        }
        return mItemsNews;
    }

    public void getData() {
        mItemsNews = mHandleXml.getItemsNews();
        updateChannel();
        getItemsFromDb(mItemsNews);
    }

    /**
     * Exception processing
     */
    public void processingException(String message) {
        Bundle bundle = new Bundle();
        bundle.putString("messageException", message);
        Message msg = new Message();
        msg.setData(bundle);
        msg.what = 0;
        mHandler.sendMessage(msg);
    }

    public ArrayList<ItemNews> getItemsFromDb(ArrayList<ItemNews> itemsNews) {
        ArrayList<String> data = new ArrayList<String>();
        data.add(mTableName);
        itemsNews = mDataBase.getAllItemsNews(data, itemsNews);
        mDataBase.setTagSeparator(itemsNews);
        mDataBase.addRec(itemsNews, mTableName);
        return itemsNews;
    }

    /**
     * Updates channel in table steams
     */
    public void updateChannel() {
        StreamItemChannel streamChannel = new StreamItemChannel();
        streamChannel = mHandleXml.getStreamChannel();
        if (mDataBase.searchData(streamChannel.getChannelTitle(), DB.TABLE_STREAM, DB.COLUMN_STREAM_NAME) != true) {
            mDataBase.updateTableStream(streamChannel);
        }
    }

}