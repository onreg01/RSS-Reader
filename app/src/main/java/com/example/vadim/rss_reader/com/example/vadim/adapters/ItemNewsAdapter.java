package com.example.vadim.rss_reader.com.example.vadim.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vadim.rss_reader.com.example.vadim.activities.ActivityNews;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.R;


public class ItemNewsAdapter extends BaseItemNewsAdapter {

    protected DB db;
    protected ArrayList<ItemNews> items;

    public ItemNewsAdapter(Context context, ArrayList<ItemNews> items,DB db) {
        super(context, items);
        this.db=db;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = lInflater.inflate(R.layout.item_news, parent, false);

        findView(convertView, position);

        return convertView;
    }

    public void findView(View convertView, int position) {
        ItemNews news = getItemNews(position);

        TextView text1, text2, text3;
        WebView webView;
        ImageView imageView;
        text1 = (TextView) convertView.findViewById(R.id.tvText);
        text2 = (TextView) convertView.findViewById(R.id.tvText1);
        text3 = (TextView) convertView.findViewById(R.id.tvText2);

        webView = (WebView) convertView.findViewById(R.id.tvWeb);
        imageView = (ImageView) convertView.findViewById(R.id.ivImage);

        setFavoriteImage(news, imageView, position);
        setWebView(webView, news);
        setTitle(text2, news, position);
        setViewPubDate(text1, news);

        text3.setText(news.getTime());
    }

    public void setViewPubDate(TextView textView, ItemNews news) {

        String tag = news.getTag();
        textView.setText(news.getDate());
        if (tag.equals(DB.TAG_NO)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }
    }

    public void setFavoriteImage(ItemNews news, ImageView image, int position) {
        if (news.getTagFavorite().equals("add")) {
            image.setImageResource(R.drawable.favorite_add);
        } else {
            image.setImageResource(R.drawable.favorite_no);
        }
        image.setOnClickListener(this);
        image.setTag(position);
    }

    public void setTitle(TextView textClick, ItemNews news, int position) {
        textClick.setText(news.getTitle());
        textClick.setOnClickListener(this);
        textClick.setTag(position);

    }

    public void setWebView(WebView webView, ItemNews item) {
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        String url = item.getDescription();
        String summaryUrl = "<html><body><font size='3' color='White'>".concat(url).concat("</font></body></html>");
        webView.loadDataWithBaseURL(null, summaryUrl, "text/html", "en_US", null);
        webView.setBackgroundColor(0);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvText1:
                titleClick(view);
                break;
            case R.id.ivImage:
                imageClick(view);
                break;
        }
    }
    /**
     * Opens the new Activity with WebView,when pressing a title news
     */
    public void titleClick(View view) {
        Intent intent = new Intent(context, ActivityNews.class);
        intent.putExtra("url", (getItemNews((Integer) view.getTag()).getLink()));
        intent.putExtra("name", (getItemNews((Integer) view.getTag()).getTitle()));
        context.startActivity(intent);
    }

    /**
     * Changes image star and update data base;
     */
    public void imageClick(View view) {
        ItemNews item = getItemNews((Integer) view.getTag());
        ImageView image = (ImageView) view.findViewById(R.id.ivImage);
        if (item.getTagFavorite().equals("add")) {
            image.setImageResource(R.drawable.favorite_no);
            item.setTagFavorite("no");
        } else {
            image.setImageResource(R.drawable.favorite_add);
            item.setTagFavorite("add");
        }
        db.updateFavorite(item);
    }


}


















