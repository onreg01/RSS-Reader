package com.example.vadim.rss_reader.com.example.vadim.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;

import java.util.ArrayList;

public class BaseItemNewsAdapter extends BaseAdapter implements View.OnClickListener {
    protected Context context;
    protected LayoutInflater lInflater;
    protected ArrayList<ItemNews> items;

    public BaseItemNewsAdapter(Context context, ArrayList<ItemNews> items) {
        this.context = context;
        this.items = items;
        this.lInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return convertView;
    }

    public ItemNews getItemNews(int position) {
        return ((ItemNews) getItem(position));
    }

    @Override
    public void onClick(View view) {
    }

    public ArrayList<ItemNews> getNews() {
        return items;
    }

    public void removeList() {
        items.clear();
    }

    public void removeItem(int position) {
        items.remove(position);
    }
}
