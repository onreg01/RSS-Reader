package com.example.vadim.rss_reader.com.example.vadim.exception;

public class IziException extends Exception {
    public static final String NO_NEW_NEWS = "no_new_news";
    public static final String URL_ALREADY_USED = "url_is_already_used";


    public IziException(String detailMessage) {
        super(detailMessage);
    }
}
