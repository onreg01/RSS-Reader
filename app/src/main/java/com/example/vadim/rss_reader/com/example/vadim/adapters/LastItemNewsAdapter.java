package com.example.vadim.rss_reader.com.example.vadim.adapters;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;

import java.util.ArrayList;

public class LastItemNewsAdapter extends FavoriteItemNewsAdapter {
    public LastItemNewsAdapter(Context context, ArrayList<ItemNews> items, DB db, ArrayList<StreamItemChannel> streamItemChannels) {
        super(context, items, db, streamItemChannels);
    }

    /**
     * Changes image star and update data base;
     */
    public void imageClick(View view) {
        ItemNews item = getItemNews((Integer) view.getTag());
        ImageView image = (ImageView) view.findViewById(R.id.ivImage);
        if (item.getTagFavorite().equals("add")) {
            image.setImageResource(R.drawable.favorite_no);
            item.setTagFavorite("no");
        } else {
            image.setImageResource(R.drawable.favorite_add);
            item.setTagFavorite("add");
        }
        db.updateFavorite(item);
    }
}
