package com.example.vadim.rss_reader.com.example.vadim.objects;


public class StreamItemChannel {


    private String tableName;
    private String imageUrl;
    private String channelTitle;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String image) {
        this.imageUrl = image;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String ChannelTitle) {
        this.channelTitle = ChannelTitle;
    }
}
