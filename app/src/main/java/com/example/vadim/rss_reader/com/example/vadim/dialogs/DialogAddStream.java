package com.example.vadim.rss_reader.com.example.vadim.dialogs;

import android.app.Activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.exception.IziException;

public class DialogAddStream extends DialogFragment implements OnClickListener {
    EditDialogListener editDialogListener;
    private EditText mTextUrl;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog, null);
        view.findViewById(R.id.tvButtonAccept).setOnClickListener(this);
        view.findViewById(R.id.tvButtonCancel).setOnClickListener(this);
        mTextUrl = (EditText) view.findViewById(R.id.tvEditTextUrl);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.backGroundColor)));
        getDialog().setTitle(getResources().getString(R.string.new_steam));
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
           editDialogListener = (EditDialogListener) activity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvButtonAccept:
                buttonAccept();
                break;
            case R.id.tvButtonCancel:
                dismiss();
                break;
        }
    }

    /**
     * Gets url from text fields and try add new channel
     */
    public void buttonAccept() {
        if (TextUtils.isEmpty(mTextUrl.getText().toString())) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.cant_be_empty), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            editDialogListener.onFinishEditDialog(mTextUrl.getText().toString());
            dismiss();
        } catch (IziException exception) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.already_used), Toast.LENGTH_SHORT).show();
        }
    }

    public interface EditDialogListener {
        public void onFinishEditDialog(String inputText) throws IziException;
    }

    public void test1()
    {

    }

    public void test2()
    {

    }

    public void test3()
    {

    }

    public void test4()
    {

    }

    public void test5()
    {

    }

    public void test6()
    {

    }
}