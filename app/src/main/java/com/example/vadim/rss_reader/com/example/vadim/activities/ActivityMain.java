package com.example.vadim.rss_reader.com.example.vadim.activities;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.dialogs.DialogAddStream;
import com.example.vadim.rss_reader.com.example.vadim.exception.IziException;
import com.example.vadim.rss_reader.com.example.vadim.fragments.FragmentChannels;
import com.example.vadim.rss_reader.com.example.vadim.fragments.FragmentFavoriteNews;
import com.example.vadim.rss_reader.com.example.vadim.fragments.FragmentLastNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.streams.BackGround;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ActivityMain extends ActionBarActivity implements ActionBar.TabListener, LoaderManager.LoaderCallbacks<Cursor>,
        FragmentChannels.FragmentChannelInterface, DialogAddStream.EditDialogListener {
    private ActionBar mActionBar;
    private FragmentChannels mFragmentChannels;
    private DB mDataBase;
    private Integer mPosition;
    private Long mId;
    private ActionMode mActionMode;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDataBase = DB.getDataBaseObject(this);
        mDataBase.open();
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        String[] titleBar = {getResources().getString(R.string.channels), getResources().getString(R.string.favorite),
                getResources().getString(R.string.last_news)};
        for (String title : titleBar) {
            setTab(title);
        }
        mHandler = new MyHandler(this);
    }

    public void setTab(String title) {
        ActionBar.Tab tab = mActionBar.newTab();
        tab.setText(title);
        tab.setTabListener(this);
        mActionBar.addTab(tab);
    }

    /**
     * Creates menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mId != null) {
            mActionMode = startSupportActionMode(new SelectedItems(mPosition, mId));
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tabState", mActionBar.getSelectedTab().getPosition());
        if (mPosition != null) {
            outState.putInt("pos", mPosition);
            outState.putLong("id", mId);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mActionBar.setSelectedNavigationItem(savedInstanceState.getInt("tabState"));
        if (savedInstanceState.getInt("pos", 999999999) != 999999999) {
            mPosition = savedInstanceState.getInt("pos");
            mId = savedInstanceState.getLong("id");
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        switch (tab.getPosition()) {
            case 0:
                mFragmentChannels = new FragmentChannels();
                ft.replace(R.id.fragmentContainer, mFragmentChannels);
                mFragmentChannels.setHasOptionsMenu(true);
                break;
            case 1:
                FragmentFavoriteNews fragmentFavoriteNews = new FragmentFavoriteNews();
                ft.replace(R.id.fragmentContainer, fragmentFavoriteNews);
                break;
            case 2:
                FragmentLastNews fragmentLastNews = new FragmentLastNews();
                ft.replace(R.id.fragmentContainer, fragmentLastNews);
                break;

        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(this, mDataBase);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mFragmentChannels.setAdapter(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public DB getDataBase() {
        return mDataBase;
    }

    @Override
    public void startActionMode(int position, Long id) {
        mActionMode = startSupportActionMode(new SelectedItems(position, id));
    }

    @Override
    public void createLoader() {
        getSupportLoaderManager().initLoader(0, null, this);
    }


    @Override
    public void showDialog() {
        android.support.v4.app.DialogFragment dialogAddStream = new DialogAddStream();
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialogAddStream.show(ft, "dialog");
    }

    /**
     * Checked url in data base, abort if true or add new channel
     */
    @Override
    public void onFinishEditDialog(String url) throws IziException {
        if (mDataBase.searchData(url, DB.TABLE_STREAM, DB.COLUMN_STREAM_URL)) {
            throw new IziException(IziException.URL_ALREADY_USED);
        }
        mDataBase.addStream(url);
        getSupportLoaderManager().getLoader(0).forceLoad();
    }

    /**
     * Returns false if no connection to network, else true
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        mDataBase.close();
        ActivityMain.super.onBackPressed();

    }

    /**
     * Class for work cursor loader
     */
    static class MyCursorLoader extends CursorLoader {

        private DB mDataBase;

        public MyCursorLoader(Context context, DB dataBase) {
            super(context);
            this.mDataBase = dataBase;
        }

        /**
         * Gets cursor with data
         */
        @Override
        public Cursor loadInBackground() {
            Cursor cursor = mDataBase.getCursorStreams();
            return cursor;
        }
    }

    /**
     * Class receives messages from streams
     */
    static class MyHandler extends Handler {

        static WeakReference<ActivityMain> wrActivity;

        public MyHandler(ActivityMain activityMain) {
            wrActivity = new WeakReference<ActivityMain>(activityMain);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ActivityMain activity = wrActivity.get();
            activity.mFragmentChannels.disableProgressBar();
            switch (msg.getData().getString("messageException")) {
                case "no_new_news":
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_new_news), Toast.LENGTH_SHORT).show();
                    break;
                case "error":
                    Toast.makeText(activity, activity.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    break;
                case "wrong_url":
                    Toast.makeText(activity, activity.getResources().getString(R.string.wrong_url), Toast.LENGTH_SHORT).show();

            }
        }
    }

    /**
     * Class realizes work action mode
     */
    private class SelectedItems implements ActionMode.Callback {
        private int pos;
        private Long id;


        private SelectedItems(int position, Long id) {
            this.pos = position;
            this.id = id;
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.actionmode_main_activity, menu);
            mPosition = pos;
            mId = id;
            mFragmentChannels.setViewColor(mPosition);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.remove_channel:
                    mDataBase.delStream(mId);
                    getSupportLoaderManager().getLoader(0).forceLoad();
                    break;
                case R.id.refresh_news:
                    if (isNetworkConnected() != false) {
                        modeRefreshNews();
                    } else {
                        Toast.makeText(getBaseContext(), "Network Error", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
            actionMode.finish();
            return false;
        }

        public void modeRefreshNews() {
            mFragmentChannels.setProgressBar();
            ArrayList<String> data = new ArrayList<>();
            String columns[] = {DB.COLUMN_STREAM_NAME, DB.COLUMN_STREAM_URL, DB.COLUMN_STREAM_TABLE_NAME};
            for (String column : columns) {
                mDataBase.getDataStream(column, mId, data);
            }
            TaskLoader taskLoader = new TaskLoader(data.get(1), data.get(2));
            taskLoader.load();
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mFragmentChannels.returnViewColor(mPosition);
            mPosition = null;
            mId = null;
        }
    }

    @Override
    public void closeActionMode()
    {
        if(mActionMode!=null)
        {
            mActionMode.finish();
        }
    }

    public class TaskLoader implements LoaderManager.LoaderCallbacks<ArrayList<ItemNews>> {

        private String mUrl, mTableName;

        public TaskLoader(String mUrl, String mTableName) {
            this.mUrl = mUrl;
            this.mTableName = mTableName;
        }


        public void load() {
            getSupportLoaderManager().restartLoader(1, null, this);
            getSupportLoaderManager().getLoader(1).forceLoad();
        }


        @Override
        public Loader<ArrayList<ItemNews>> onCreateLoader(int id, Bundle args) {
            return new BackGround(getBaseContext(), mUrl, mTableName, mDataBase, mHandler);
        }

        @Override
        public void onLoadFinished(Loader<ArrayList<ItemNews>> loader, ArrayList<ItemNews> itemsNews) {
            mFragmentChannels.disableProgressBar();
            getSupportLoaderManager().getLoader(0).forceLoad();
        }

        @Override
        public void onLoaderReset(Loader<ArrayList<ItemNews>> loader) {

        }
    }
}
