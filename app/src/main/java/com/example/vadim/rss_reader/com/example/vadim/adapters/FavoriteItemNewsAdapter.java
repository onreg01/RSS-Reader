package com.example.vadim.rss_reader.com.example.vadim.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class FavoriteItemNewsAdapter extends ItemNewsAdapter {
    protected ArrayList<StreamItemChannel> streamItemChannels;
    protected DisplayImageOptions options;
    protected ImageLoader imageLoader;
    protected ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    public FavoriteItemNewsAdapter(Context context, ArrayList<ItemNews> items, DB db, ArrayList<StreamItemChannel> streamItemChannels) {
        super(context, items, db);
        this.streamItemChannels = streamItemChannels;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemNews news = getItemNews(position);
        View view = super.getView(position, convertView, parent);

        String fromTable = news.getFromTable();
        for (StreamItemChannel channel : streamItemChannels) {
            if (fromTable.equals(channel.getTableName())) {
                setText(view, channel);
                setImage(view, channel);
            }
        }
        return view;
    }

    public void setText(View view, StreamItemChannel channel) {
        TextView text = (TextView) view.findViewById(R.id.tvFromChannel);
        text.setText(channel.getChannelTitle());
    }

    public void setImage(View view, StreamItemChannel channel) {
        ImageView image = (ImageView) view.findViewById(R.id.imageViewFavorite);
        String imageUrl = channel.getImageUrl();

        if (imageUrl.equals("null")) {
            image.setVisibility(View.GONE);
        } else {
            initializationImageLoader(imageUrl, image);
        }
    }

    public void initializationImageLoader(String imageUrl, ImageView image) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        imageLoader.displayImage(imageUrl, image, options, animateFirstListener);
    }

    public void setOptions() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    /**
     * Adds to favorite news when pressing on image
     */
    public void imageClick(View view) {
        ItemNews item = getItemNews((Integer) view.getTag());
        item.setTagFavorite("no");
        db.updateFavorite(item);
        removeItem(((Integer) view.getTag()));
        notifyDataSetChanged();
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}
