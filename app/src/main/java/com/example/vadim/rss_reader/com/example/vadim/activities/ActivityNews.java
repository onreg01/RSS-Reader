package com.example.vadim.rss_reader.com.example.vadim.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.vadim.rss_reader.R;


public class ActivityNews extends ActionBarActivity {

    private  WebView webView;
    /**
     * Gets url from intent and begins webView with this ulr
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_news);
        webView = (WebView) findViewById(R.id.wvWebNews);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new simpleWebClien());
        webView.getSettings().setBuiltInZoomControls(true);

        webView.loadUrl(intent.getStringExtra("url"));
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
//            webView.goBack();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    private class simpleWebClien extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }
    }
}
