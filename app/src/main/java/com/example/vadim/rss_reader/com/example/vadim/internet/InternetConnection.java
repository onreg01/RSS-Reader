package com.example.vadim.rss_reader.com.example.vadim.internet;

import com.example.vadim.rss_reader.com.example.vadim.exception.FatalException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class InternetConnection {
    private String mUrl;
    private InputStream mStream;
    private HttpURLConnection mConnection;


    public InternetConnection(String url) {
        this.mUrl = url;

    }

    public InputStream createConnection() throws FatalException {
        try {
            URL url = new URL(mUrl);
            mConnection = (HttpURLConnection) url.openConnection();
            mConnection.setReadTimeout(10000 /* milliseconds */);
            mConnection.setConnectTimeout(15000 /* milliseconds */);
            mConnection.setRequestMethod("GET");
            mConnection.setDoInput(true);
            mConnection.connect();
            mStream = mConnection.getInputStream();

        } catch (MalformedURLException exception) {
            throw new FatalException(FatalException.WRONG_URL);

        } catch (IOException exception) {
            throw new FatalException(FatalException.ERROR);
        }
        return mStream;
    }


    public void closeConnection() throws FatalException {
        try {
            mStream.close();
            mConnection.disconnect();
        } catch (IOException e) {
            throw new FatalException(FatalException.ERROR);
        }
    }

}
