package com.example.vadim.rss_reader.com.example.vadim.fragments;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.activities.ActivityStream;
import com.example.vadim.rss_reader.com.example.vadim.adapters.ChannelCursorAdapter;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;

import java.util.ArrayList;

public class FragmentChannels extends Fragment{
    private ListView mListData;
    private FragmentChannelInterface fragmentChannelInterface;
    private DB mDataBase;
    private boolean mProgress =  false;
    ChannelCursorAdapter channelAdapter;



    public interface FragmentChannelInterface{
        public void createLoader();
        public DB getDataBase();
        public void startActionMode(int position,Long id);
        public void showDialog();
        public void closeActionMode();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentChannelInterface = (FragmentChannelInterface) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channels, container, false);
        setListView(view);
        mDataBase = fragmentChannelInterface.getDataBase();
        fragmentChannelInterface.createLoader();
        return view;
    }

    /**
     * Creates menu
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main_activity, menu);
       if(mProgress==true) {
           MenuItemCompat.setActionView(menu.findItem(R.id.prog), (R.layout.progress));
       }

    }

    public void setProgressBar(){
        mProgress=true;
        getActivity().supportInvalidateOptionsMenu();
    }

    public void disableProgressBar(){
        mProgress=false;
        getActivity().supportInvalidateOptionsMenu();
    }

    /**
     * Creates and show DialogFragment
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_channel:
                fragmentChannelInterface.showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setListView(View view) {
        mListData = (ListView) view.findViewById(R.id.lvData);
        mListData.setOnItemClickListener(new ListViewItemListener());
        mListData.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView parent, View view, int position, long id) {
                fragmentChannelInterface.startActionMode(position,id);
                return true;
            }
        });
    }

    public void setAdapter(Cursor cursor){
        channelAdapter = new ChannelCursorAdapter(getActivity(), cursor, 0);
        mListData.setAdapter(channelAdapter);
    }

    public void setViewColor(int position){
        mListData.setItemChecked(position, true);
    }

    public void returnViewColor(int position){
        mListData.setItemChecked(position, false);
}

    public class ListViewItemListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            returnViewColor(position);
            fragmentChannelInterface.closeActionMode();
            ArrayList<String> data = new ArrayList<>();
            String columns[] = {DB.COLUMN_STREAM_NAME,DB.COLUMN_STREAM_TABLE_NAME};
            for (String column : columns) {
                mDataBase.getDataStream(column, id, data);
            }
            Intent intent = new Intent(getActivity(), ActivityStream.class);
            intent.putStringArrayListExtra("data", data);
            startActivity(intent);
        }
    }
}
