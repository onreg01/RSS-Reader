package com.example.vadim.rss_reader.com.example.vadim.objects;


import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


public class ItemNews implements Comparable {
    private static SimpleDateFormat[] formats =
            new SimpleDateFormat[]{new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH),
                    new SimpleDateFormat("dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)};
    private String title;
    private String link;
    private String description;
    private String time;
    private String date;
    private String tagSeparator;
    private String tagFavorite;
    private String fromTable;
    private String dateMilliseconds;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTag() {
        return tagSeparator;
    }

    public void setTag(String tagSeparator) {
        this.tagSeparator = tagSeparator;
    }

    public String getTagFavorite() {
        return tagFavorite;
    }

    public void setTagFavorite(String favorite) {
        this.tagFavorite = favorite;
    }

    public String getFromTable() {
        return fromTable;
    }

    public void setFromTable(String fromTable) {
        this.fromTable = fromTable;
    }

    public void setDateMilliseconds(String dateMilliseconds) {
        this.dateMilliseconds = dateMilliseconds;
    }

    public String getDateMilliseconds() {
        return dateMilliseconds;
    }

    public void setParseTime(String time) {
        Calendar cal = new GregorianCalendar();
        Date parsedDate = null;
        for (int i = 0; i < formats.length; i++) {
            try {
                parsedDate = formats[i].parse(time);
                cal.setTime(parsedDate);
                String hour = Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
                String minute = Integer.toString(cal.get(Calendar.MINUTE));
                if (minute.length() == 1) {
                    minute = "0".concat(minute);
                }
                time = hour.concat(":").concat(minute);
                setTime(time);
                return;
            } catch (ParseException e) {
                continue;
            }
        }
        setTime("");
    }

    public void setParseDate(String pubDate) {
        Calendar cal = new GregorianCalendar();
        Date parsedDate = null;
        for (int i = 0; i < formats.length; i++) {
            try {
                parsedDate = formats[i].parse(pubDate);
                cal.setTime(parsedDate);
              //  cal.add(Calendar.DATE, -40);
                String day = DB.WEEK[cal.get(Calendar.DAY_OF_WEEK) - 1];
                String dayOfMonth = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
                String month = DB.YEAR[cal.get(Calendar.MONTH)];
                String year = Integer.toString(cal.get(Calendar.YEAR));
                String date = day.concat(", ").concat(dayOfMonth).concat(" ").concat(month).concat(" ").concat(year);
                setDate(date);
                setDateMilliseconds(Long.toString(cal.getTimeInMillis()));
                return;
            } catch (ParseException e) {
                continue;
            }
        }
        setDateMilliseconds("");
        setDate("");
    }

    @Override
    public int compareTo(Object ItemNews) {
        ItemNews item = (ItemNews) ItemNews;
        if (Long.valueOf(this.dateMilliseconds) < Long.valueOf(item.getDateMilliseconds())) {
            return 1;
        } else if (Long.valueOf(this.dateMilliseconds) > Long.valueOf(item.getDateMilliseconds())) {
            return -1;
        }
        return 0;
    }
}