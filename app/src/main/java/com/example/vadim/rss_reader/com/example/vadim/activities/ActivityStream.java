package com.example.vadim.rss_reader.com.example.vadim.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.vadim.rss_reader.com.example.vadim.streams.BackGround;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.adapters.ItemNewsAdapter;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ActivityStream extends ActionBarActivity implements SearchView.OnQueryTextListener {
    private ArrayList<ItemNews> mItems = new ArrayList<>();
    private ItemNewsAdapter mList;
    private ListView mLvStream;
    private DB mDataBase;
    private String mTableName = null;
    private SearchView mSearchView;
    private String savedText;

    /**
     * Gets data base object, fills a collection of objects
     * create the adapter and AsyncTaskLoader
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);
        mLvStream = (ListView) findViewById(R.id.lvDataStream);
        mDataBase = DB.getDataBaseObject(this);

        setIntentData();

        ArrayList<String> tables = new ArrayList<String>();
        tables.add(mTableName);

        mItems = mDataBase.getAllItemsNews(tables, mItems);
        if (mItems.isEmpty() != true) {
            setAdapter(mItems);
        }

    }

    /**
     * Gets "url" and "table name" from Intent, and set data
     */
    public void setIntentData() {
        Intent intent = getIntent();
        ArrayList<String> data = new ArrayList<String>(intent.getStringArrayListExtra("data"));
        String streamName = data.get(0);
        mTableName = data.get(1);
        setTitle(streamName);
    }

    /**
     * Creates menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_stream, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.setQueryHint(getResources().getString(R.string.action_search));
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setQuery(savedText, false);

        return true;
    }

    /**
     * Work menu points
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        mList.removeList();
        mDataBase.delAllRec(mTableName);
        mLvStream.setAdapter(null);

        return true;
    }

    /**
     * Saves the status "mSearchView" and "mMenuEnabled"
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("search", mSearchView.getQuery().toString());
    }

    /**
     * Restore the status "mSearchView" and "mMenuEnabled"
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedText = savedInstanceState.getString("search");
        supportInvalidateOptionsMenu();
    }

    public void setAdapter(ArrayList<ItemNews> itemList) {
        mList = new ItemNewsAdapter(this, itemList, mDataBase);
        mLvStream.setAdapter(mList);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    /**
     * Work mSearchView
     */
    @Override
    public boolean onQueryTextChange(String searchText) {
        ArrayList<ItemNews> searchItems = new ArrayList<>();
        int textLength = searchText.length();
        for (ItemNews item : mItems) {
            if (textLength <= item.getTitle().length()) {
                if (searchText.equalsIgnoreCase(item.getTitle().substring(0, textLength))) {
                    searchItems.add(item);
                }
            }
        }
        setAdapter(searchItems);
        return false;
    }
}