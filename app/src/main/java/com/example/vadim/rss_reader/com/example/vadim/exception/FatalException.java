package com.example.vadim.rss_reader.com.example.vadim.exception;


public class FatalException extends Exception {
    public static final String ERROR = "error";
    public static final String WRONG_URL = "wrong_url";

    public FatalException(String detailMessage) {
        super(detailMessage);
    }
}
