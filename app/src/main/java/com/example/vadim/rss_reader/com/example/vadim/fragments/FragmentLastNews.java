package com.example.vadim.rss_reader.com.example.vadim.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.example.vadim.rss_reader.R;
import com.example.vadim.rss_reader.com.example.vadim.adapters.LastItemNewsAdapter;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;

import java.util.ArrayList;


public class FragmentLastNews extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_last_news, null);

        DB db = DB.getDataBaseObject(getActivity());

        ListView favoriteNews = (ListView) view.findViewById(R.id.lvLastNews);
        ArrayList<ItemNews> itemsNews = new ArrayList<>();

        itemsNews = db.getAllLastItemsNews();
        db.setTagSeparator(itemsNews);

        ArrayList<StreamItemChannel> streamItemChannels = new ArrayList<>();
        streamItemChannels = db.getAllChannels();

        LastItemNewsAdapter listAdapter = new LastItemNewsAdapter(getActivity(), itemsNews, db, streamItemChannels);
        favoriteNews.setAdapter(listAdapter);
        return view;
    }


}
