package com.example.vadim.rss_reader.com.example.vadim.parsing;

import com.example.vadim.rss_reader.com.example.vadim.exception.FatalException;
import com.example.vadim.rss_reader.com.example.vadim.exception.IziException;
import com.example.vadim.rss_reader.com.example.vadim.data_base.DB;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class HandleXML {


    private String mItemTag = null;
    private ArrayList<ItemNews> mItems = new ArrayList<ItemNews>();
    private StreamItemChannel mStreamChanel = new StreamItemChannel();
    private String mText;
    private DB mDataBase;
    private XmlPullParserFactory mXmlFactoryObject;
    private String mTableName;
    private String mTagName;

    public HandleXML(DB DataBase, String tableName) {
        this.mDataBase = DataBase;
        this.mTableName = tableName;
    }


    public void setParser(InputStream stream) throws IziException, FatalException {
        try {
            mXmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser xmlPullParser = mXmlFactoryObject.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(stream, null);
            parseXMLAndStoreIt(xmlPullParser);
        } catch (XmlPullParserException exception) {
            throw new FatalException(FatalException.ERROR);
        }
    }

    /**
     * fills the collection "mItems" from RSS
     */
    public void parseXMLAndStoreIt(XmlPullParser myParser) throws IziException, FatalException {
        int event;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                mTagName = myParser.getName();
                startEvent(event, myParser);
                event = myParser.next();
            }
        } catch (XmlPullParserException e) {
            throw new FatalException(FatalException.ERROR);
        } catch (IOException e) {
            throw new FatalException(FatalException.ERROR);
        }
    }

    public void startEvent(int event, XmlPullParser myParser) throws IziException {
        switch (event) {
            case XmlPullParser.START_TAG:
                startTag();
                break;
            case XmlPullParser.TEXT:
                mText = myParser.getText();
                break;
            case XmlPullParser.END_TAG:
                endTag();
                break;
        }
    }

    public void startTag() {
        if (mTagName.equals("channel")) {
            mItemTag = "channel";
            new StreamItemChannel();
        } else if (mTagName.equals("item")) {
            mItemTag = "item";
        } else if (mTagName.equals("image")) {
            mItemTag = "image";
        }
    }

    public void endTag() throws IziException {
        if (mItemTag.equals("channel")) {
            switch (mTagName) {
                case "title":
                    mStreamChanel.setTableName(mTableName);
                    mStreamChanel.setChannelTitle(mText);
                    break;
            }
        } else if (mItemTag.equals("image")) {
            if (mTagName.equals("url")) {
                mStreamChanel.setImageUrl(mText);
            }

        } else if (mItemTag.equals("item")) {
            endTagItem();
        }
    }

    public void endTagItem() throws IziException {
        switch (mTagName) {
            case "title":
                endTagItemTitle();
                break;
            case "link":
                mItems.get(mItems.size() - 1).setLink(mText);
                break;
            case "description":
                mItems.get(mItems.size() - 1).setDescription(mText);
                break;
            case "pubDate":
                mItems.get(mItems.size() - 1).setParseTime(mText);
                mItems.get(mItems.size() - 1).setParseDate(mText);
                break;
        }
    }

    public void endTagItemTitle() throws IziException {
        if (mDataBase.searchData(mText, mTableName, DB.COLUMN_TITLE)) {
            throw new IziException(IziException.NO_NEW_NEWS);
        }
        mItems.add(new ItemNews());
        mItems.get(mItems.size() - 1).setFromTable(mTableName);
        mItems.get(mItems.size() - 1).setTitle(mText);
        mItems.get(mItems.size() - 1).setTagFavorite("no");
    }

    public ArrayList<ItemNews> getItemsNews() {
        return mItems;
    }

    public StreamItemChannel getStreamChannel() {
        return mStreamChanel;
    }

}