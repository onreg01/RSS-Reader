package com.example.vadim.rss_reader.com.example.vadim.data_base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.vadim.rss_reader.com.example.vadim.objects.ItemNews;
import com.example.vadim.rss_reader.com.example.vadim.objects.StreamItemChannel;

public class DB {
    private static DB mDataBase;

    public static final String DB_NAME = "dbStreams";
    public static final int DB_VERSION = 1;

    public static final String TABLE_STREAM = "Stream_Table";
    public static final String COLUMN_STREAM_ID = "_id";
    public static final String COLUMN_STREAM_URL = "Stream_Url";
    public static final String COLUMN_STREAM_NAME = "Stream_Name";
    public static final String COLUMN_STREAM_TABLE_NAME = "Stream_Table_Name";
    public static final String COLUMN_IMAGE = "image";


    public static final String DB_CREATE_STREAM =
            "create table " + TABLE_STREAM + "(" +
                    COLUMN_STREAM_ID + " integer primary key autoincrement, " +
                    COLUMN_STREAM_NAME + " text, " +
                    COLUMN_STREAM_URL + " text, " +
                    COLUMN_STREAM_TABLE_NAME + " text, " +
                    COLUMN_IMAGE + " text " +
                    ");";

    public static String sTableNews = null;
    public static final String COLUMN_FROM_TABLE = "from_table";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_DATE = "pubdate";
    public static final String COLUMN_DATE_IN_MILLISECONDS = "miliseconds";
    public static final String COLUMN_TAG_SEPARATOR = "seporator";
    public static final String COLUMN_TAG_FAVORITE = "favorite";

    public static final String TAG_YES = "yes";
    public static final String TAG_NO = "no";

    public static String[] YEAR = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static String[] WEEK = {"Sun", "Mun", "Tue", "Wed", "Thu", "Fri", "Sat"};

    private Context mCtx;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    private DB(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Returns data base object
     */
    public static DB getDataBaseObject(Context con) {
        if (mDataBase == null) {
            mDataBase = new DB(con);
        }
        return mDataBase;
    }

    /**
     * Opens connection to data base
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closes connection to data base
     */
    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    /**
     * ADD RSS stream
     */
    public void addStream(String url) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_STREAM_NAME, url);
        cv.put(COLUMN_STREAM_URL, url);
        String tableName = "'".concat(url).concat("'");
        cv.put(COLUMN_STREAM_TABLE_NAME, tableName);
        cv.put(COLUMN_IMAGE, "null");

        mDB.insert(TABLE_STREAM, null, cv);
        createTableStream(tableName);
    }

    public void updateTableStream(StreamItemChannel channel) {
        String fromTable = channel.getTableName();
        String channelImage = channel.getImageUrl();
        String channelTitle = channel.getChannelTitle();

        ContentValues cv = new ContentValues();
        if (channelImage != null) {
            cv.put(COLUMN_IMAGE, channelImage);
        }
        cv.put(COLUMN_STREAM_NAME, channelTitle);

        mDB.update(TABLE_STREAM, cv, COLUMN_STREAM_TABLE_NAME.concat(" = ?"), new String[]{fromTable});
    }

    /**
     * Deletes RSS stream
     */
    public void delStream(long id) {
        ArrayList<String> data = new ArrayList<>();
        data = getDataStream(COLUMN_STREAM_TABLE_NAME, id, data);
        mDB.delete(TABLE_STREAM, COLUMN_STREAM_ID + " = " + id, null);
        mDB.execSQL("DROP TABLE IF EXISTS ".concat(data.get(data.size() - 1)));
    }

    public Cursor getCursorStreams() {
        return mDB.query(TABLE_STREAM, null, null, null, null, null, null);
    }

    /**
     * Returns false if record is absent in table, else true
     */
    public boolean searchData(String text, String tableName, String columName) {
        boolean rezult = false;
        String[] columns = {columName};
        Cursor cursor = mDB.query(tableName, columns, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    for (String cn : cursor.getColumnNames()) {
                        String str = "";
                        str = str.concat(cursor.getString(cursor.getColumnIndex(cn)));
                        if (text.equals(str)) {
                            rezult = true;
                            return rezult;
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return rezult;
    }

    /**
     * Returns all stream items channels
     */
    public ArrayList<StreamItemChannel> getAllChannels() {
        ArrayList<StreamItemChannel> streamItemChannels = new ArrayList<>();
        String[] columns = {COLUMN_STREAM_TABLE_NAME, COLUMN_IMAGE, COLUMN_STREAM_NAME};
        Cursor cursor = mDB.query(TABLE_STREAM, columns, null, null, null, null, null);
        getChannelsFromCursor(streamItemChannels, cursor);
        return streamItemChannels;
    }

    /**
     * fills the collection StreamItemChannel and returns her
     */
    public ArrayList<StreamItemChannel> getChannelsFromCursor(ArrayList<StreamItemChannel> streamItemChannels, Cursor cursor) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    StreamItemChannel streamItemChannel = new StreamItemChannel();
                    int column = 0;
                    for (String cn : cursor.getColumnNames()) {
                        String str = "";
                        str = str.concat(cursor.getString(cursor.getColumnIndex(cn)));
                        formingChannels(column, streamItemChannel, str);
                        column++;
                    }
                    streamItemChannels.add(streamItemChannel);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return streamItemChannels;
    }

    /**
     * Forms and return streamItemChannel object
     */
    public StreamItemChannel formingChannels(int column, StreamItemChannel streamItemChannel, String str) {
        switch (column) {
            case 0:
                streamItemChannel.setTableName(str);
                break;
            case 1:
                streamItemChannel.setImageUrl(str);
                break;
            case 2:
                streamItemChannel.setChannelTitle(str);
                break;
        }
        return streamItemChannel;
    }

    /**
     * Returns all news
     */
    public ArrayList<ItemNews> getAllItemsNews(ArrayList<String> tables, ArrayList<ItemNews> itemsNews) {
        for (String table : tables) {
            Cursor cursor = mDB.query(table, null, null, null, null, null, null);
            getItemsNewsFromCursor(itemsNews, cursor);
        }
        return itemsNews;
    }

    /**
     * Returns favorite news
     */
    public ArrayList<ItemNews> getAllFavoriteItemsNews() {
        ArrayList<String> tables = new ArrayList<>();
        tables = getTables();
        ArrayList<ItemNews> itemsNews = new ArrayList<>();
        String selection = COLUMN_TAG_FAVORITE + " = ?";
        String[] selectionArgs = new String[]{"add"};
        for (String table : tables) {
            Cursor cursor = mDB.query(table, null, selection, selectionArgs, null, null, null);
            getItemsNewsFromCursor(itemsNews, cursor);
        }
        Collections.sort(itemsNews);
        return itemsNews;
    }

    /**
     * Returns last news
     */
    public ArrayList<ItemNews> getAllLastItemsNews() {
        ArrayList<String> tables = new ArrayList<>();
        tables = getTables();
        ArrayList<ItemNews> itemsNews = new ArrayList<>();
        String currentDate = formingCurrentDate();
        String selection = COLUMN_DATE + " = ?";
        String[] selectionArgs = new String[]{currentDate};
        for (String table : tables) {
            Cursor cursor = mDB.query(table, null, selection, selectionArgs, null, null, null);
            getItemsNewsFromCursor(itemsNews, cursor);
        }
        Collections.sort(itemsNews);
        return itemsNews;
    }

    public String formingCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        String day = DB.WEEK[calendar.get(Calendar.DAY_OF_WEEK) - 1];
        String dayOfMonth = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
        String month = DB.YEAR[calendar.get(Calendar.MONTH)];
        String year = Integer.toString(calendar.get(Calendar.YEAR));
        String currentDate = day.concat(", ").concat(dayOfMonth).concat(" ").concat(month).concat(" ").concat(year);
        return currentDate;
    }

    /**
     * fills the collection ItemNews and returns her
     */
    public ArrayList<ItemNews> getItemsNewsFromCursor(ArrayList<ItemNews> items, Cursor cursor) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ItemNews itemNews = new ItemNews();
                    int column = 0;
                    for (String cn : cursor.getColumnNames()) {
                        String str = cursor.getString(cursor.getColumnIndex(cn));
                        formingItemNews(column, itemNews, str);
                        column++;
                    }
                    items.add(itemNews);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return items;
    }

    /**
     * Forms and return streamItemChannel object
     */
    public ItemNews formingItemNews(int column, ItemNews itemNews, String str) {
        switch (column) {
            case 0:
                itemNews.setFromTable(str);
                break;
            case 1:
                itemNews.setTitle(str);
                break;
            case 2:
                itemNews.setLink(str);
                break;
            case 3:
                itemNews.setDescription(str);
                break;
            case 4:
                itemNews.setTime(str);
                break;
            case 5:
                itemNews.setDate(str);
                break;
            case 6:
                itemNews.setDateMilliseconds(str);
                break;
            case 7:
                itemNews.setTag(str);
                break;
            case 8:
                itemNews.setTagFavorite(str);
                break;
        }
        return itemNews;
    }

    /**
     * Adds records about news
     */
    public void addRec(ArrayList<ItemNews> items, String name) {
        delAllRec(name);
        for (ItemNews item : items) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FROM_TABLE, item.getFromTable());
            cv.put(COLUMN_TITLE, item.getTitle());
            cv.put(COLUMN_LINK, item.getLink());
            cv.put(COLUMN_DESCRIPTION, item.getDescription());
            cv.put(COLUMN_TIME, item.getTime());
            cv.put(COLUMN_DATE, item.getDate());
            cv.put(COLUMN_DATE_IN_MILLISECONDS, item.getDateMilliseconds());
            cv.put(COLUMN_TAG_SEPARATOR, item.getTag());
            cv.put(COLUMN_TAG_FAVORITE, item.getTagFavorite());
            mDB.insert(name, null, cv);
        }
    }

    public void delAllRec(String name) {
        mDB.delete(name, null, null);
    }

    public void createTableStream(String name) {
        sTableNews = name;
        String DB_CREATE =
                "create table " + sTableNews + "(" +
                        COLUMN_FROM_TABLE + " text, " +
                        COLUMN_TITLE + " text, " +
                        COLUMN_LINK + " text, " +
                        COLUMN_DESCRIPTION + " text, " +
                        COLUMN_TIME + " text, " +
                        COLUMN_DATE + " text, " +
                        COLUMN_DATE_IN_MILLISECONDS + " text, " +
                        COLUMN_TAG_SEPARATOR + " text, " +
                        COLUMN_TAG_FAVORITE + " text " +
                        ");";
        mDB.execSQL(DB_CREATE);
    }

    public ArrayList<String> getDataStream(String columnStreamName, long id, ArrayList<String> data) {
        String[] columns = new String[]{columnStreamName};
        StringBuilder builder = new StringBuilder();
        builder.append("_id=");
        builder.append(Long.toString(id));
        String selection = builder.toString();
        Cursor cursor = mDB.query(TABLE_STREAM, columns, selection, null, null, null, null);
        getStringDataFromCursor(cursor, data);
        return data;
    }

    /**
     * Receives string data from cursor and return it
     */
    public ArrayList<String> getStringDataFromCursor(Cursor cursor, ArrayList<String> data) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    for (String cn : cursor.getColumnNames()) {
                        data.add((cursor.getString(cursor.getColumnIndex(cn))));
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return data;
    }

    /**
     * Returns tables steams
     */
    public ArrayList<String> getTables() {
        ArrayList<String> tables = new ArrayList<>();
        String[] columns = new String[]{COLUMN_STREAM_TABLE_NAME};
        Cursor cursor = mDB.query(TABLE_STREAM, columns, null, null, null, null, null);
        getStringDataFromCursor(cursor, tables);
        return tables;
    }

    /**
     * Update tag favorite in item news
     */
    public void updateFavorite(ItemNews item) {
        String fromTable = item.getFromTable();
        String title = item.getTitle();
        String tagFavorite = item.getTagFavorite();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TAG_FAVORITE, tagFavorite);

        mDB.update(fromTable, cv, COLUMN_TITLE.concat(" = ?"), new String[]{title});
    }

    /**
     * Sets day separator
     */
    public ArrayList<ItemNews> setTagSeparator(ArrayList<ItemNews> items) {
        String pubDate = "";
        for (ItemNews news : items) {
            if (pubDate.equals(news.getDate())) {
                news.setTag(TAG_NO);
            } else {
                news.setTag(TAG_YES);
                pubDate = news.getDate();
            }
        }
        return items;
    }

    /**
     * Class on creation and management of a database
     */
    private class DBHelper extends SQLiteOpenHelper {

        private DBHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_STREAM);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}